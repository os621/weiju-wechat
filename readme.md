## 微距

当你在城市打拼，独自在一个人 🧑 总有某些时刻你会想：为什么总是一个人？为什么自己要到这里？
手机中的社交软件一大堆，却总是找不到一个知己，以前总是认为平时一个人挺好的，可是现在一个人生病的话，就显得非常难受了。
多么想有那么一个人来照顾自己。不论性别与否，男性朋友、女性朋友、男朋友或女朋友等，多么希望有一个人来看望自己啊。☹😳

网络社交越来越发达，人与人之间的隔阂是越来越重，当然自己的圈子是越来越小了。我想解决这一痛点，便诞生出了一个想法，就比如一个人想看个电影，感觉有点奇怪。于是我想打造一个真实为前提，互帮互助的一个平台。但并不想打造一个社交平台，或者一个群体。群体一旦人一多，根本无法控制其内容。以前加过许多学习群，特别是人多的群，大多是装逼成风，毫无学习气氛。这个情况的群多半是死亡:skull:状态了。

我的想法是这样的：在平台上发表自己的诉求，一旦有人有诉求或意向寻求帮助，便可进入类似聊天室的虚拟房间进行聊天，其他人就无法进入查看其聊天内容。 
陪伴是个互相的过程，两个孤独的陌生人碰在一起（不是相亲啊，但也不反对:flushed:  ）,我孤独的时候总想出去走走玩玩，但是想想一个人有啥意思呀。我自己深有体会呀。希望·······（大家自己想想）



## 汇溪和他们的小伙伴们

打算团队叫做**汇溪**，这是以前学习时的组名。一起打造的小程序叫微距。如果可以做出来的话，打算是前台和后台全部开源。预计是半年之内做出来一个粗糙版本（先能走通一个流程）。预计2020年的3-4月份。争取做出来。

目前的话只有5位小伙伴（包括我），大家其实都挺忙的，白天毕竟要做公司的项目，下班回家的话有的估计就很累了（不打游戏就有时间了嘛Y0.0Y，周末敲代码那得需要很大的毅力了呀）。 大家愿意参加我挺高兴的，虽然代码没提交几次（稍稍吐槽）。

进度是真的缓慢呀，毕竟前端靠我一个人写（还好前段时间恶补了CSS）。页面出来才能写功能呀。前端其实并不难写呀！CSS 就那些东西，HTML 就那些东西。都是一些脚本。   **来个人也搞搞前端呗，好好恶补一下** 小程序不难的。



## 如果你想加入😝

目前**小程序** 还只是一个雏形，没有实际的功能😲。 我是个写Java的，伙伴中没有写前端的，所以小程序全靠我一个人（一个页面调一天）。后端更没时间。后端没写什么（其实自己太菜了，还没怎么写后端😅）



> Java后台地址： https://gitee.com/huixi_and_their_friends/weiju.git



|                              QQ                              |
| :----------------------------------------------------------: |
| ![](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/qrcode_1578487330214.jpg) |



## 关于整个的结构图

后端部分完成度不够，不能开源出来。

![](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/%E5%BE%AE%E8%B7%9D.png)



## 小程序原型图(16M)

> https://weiju1.oss-cn-shenzhen.aliyuncs.com/Zingo%20Social%20UI%20Kit_Dark%20theme.pdf   （PDF）



> https://weiju1.oss-cn-shenzhen.aliyuncs.com/Zingo%20Social%20UI%20Kit_Dark%20theme.sketch  (sketch)   Mac 用户可以下载这个 **sketch** 打开；Windows 可以用 **Lunacy** 打开

## 部分原型页面展示

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-16.png)

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-34.png)

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-45.png)

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-51.png)